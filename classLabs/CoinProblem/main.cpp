/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 13, 2014, 11:28 AM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    cout << "Enter how much you owe: " << endl;
    double owe;
    cin >> owe;
    
    cout << "how much did you pay? " << endl;
    double tender;
    cin >> tender;
    
    double change = tender - owe;
    
    if (change < 0 ) // didnt pay enough 
    {
        cout << "you need to give more money " << endl;
    }
    // create constants to represent currency
    const int DOLLAR = 100;
    const int QUARTER = 25;
    const int DIME = 10;
    const int NICKEL = 5;
    const int PENNY = 1;
    // convert to pennies
    change *= 100;
    //convert data type to integer
    int changeInPennies = static_cast<int>(change+ 0.5);
    cout << "Change in pennies " << changeInPennies << endl;
    
    int numDollars = changeInPennies / DOLLAR;
    changeInPennies %= DOLLAR;
    
    int numQuarters = changeInPennies / QUARTER;
    changeInPennies %= QUARTER;
    
    int numDimes = changeInPennies / DIME;
    changeInPennies %= DIME;
    int numNickels = changeInPennies / NICKEL;
    changeInPennies %= NICKEL;
    int numPennies = changeInPennies;
    
    
    cout <<"Dollars: " << numDollars << endl;
    cout << "Quarters: " << numQuarters << endl;
    cout << "Dimes: " << numDimes << endl;
    cout << "Nickels: " << numNickels << endl;
    cout << "Pennies: " << numPennies << endl;
    
           
       
    
    
    
    
    

    return 0;
}

