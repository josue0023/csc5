/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 11, 2014, 11:14 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    /* int num;
    
    // Prompt the user for input
    cout << "Enter an integer: " << endl;
    cin >> num;
    
    // determine if the # is positive or negative 
    if (num > 0)// positive
    {    
        cout << "You enter a positive #" << endl;
    }
    else if (num < 0) // negative 
    {
        cout << "You enter a negative number " << endl;
    }
    else
    {
        cout << "You entered 0 " << endl;
        
    } */
    
    /*
    //Switch cases
    int switchNum;
    cout << "Enter a number between 1-3 " << endl;
    cin >> switchNum;
    //switch acts like a menu 
    //switch only works with an integer or char
    switch (switchNum)
    {
        case 1:
        cout << "you entered number 1" << endl;
        break;
        case 2:
            cout << "you entered the number 2 " << endl;
            break;
            // can have case in jus one line 
        case 3:
            cout << "you entered the number 3 " << endl; break;
        default:
            cout << "you did not entered a valid number " << endl;
            break;
              } */
    
    //While loops 
    // needs initialization, conditions and incrementor 
    int x = 0;
    while (x < 100000000)
    {
        cout << x << endl;
        x++;
    }
            

    return 0;
}

