/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 20, 2014, 10:33 AM
 */

#include <cstdlib> //cstdlib library not needed
#include <iostream> // iostream is needed for output

// Gives the context of where my libraries are coming from 
using namespace std;

/*
 * 
 */
// There is always and only one main 
// Programs always start at main
// Programs execute from top to bottom, left to right 
int main(int argc, char** argv) {
    // cout specifies output 
    // endl creates a new line 
    // << specifies strean operateror
    // All statements end in a semicolon 
    // message is a variable
    // string is data type
    // = is an assignment operator
    // assign right to left
    
    // Using a programmer-defined identifier/variable
   // string message = "Hello world"; 
    string message ; // variable initialization 
    message = "Hello wolrd"; // variable initialization 
    
    // prompt the user
    cout<< "please enter a messsage"<< endl;
    
    cin>> message;// user input
    
    cout <<"your message is "<< message << endl;
    
    // C++ ignored white spaces
    
    cout<<"Josue Figueroa";
    // If program hits return, it ran successfully 
    
    
    return 0;
} // All my code is within curly braces

