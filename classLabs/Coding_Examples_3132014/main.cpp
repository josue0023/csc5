/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 13, 2014, 10:26 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    // Nested if Statement 
    // Determine if a # is even/odd 
    // or is the # positive/negative 
    
    //two ways
    //last way without nested statements
    // prompt the user 
    /* cout << "Enter a number : " << endl;
    int num;
    cin >> num;
    
    if (num > 0)
    {
        cout << "Positive number " << endl;
    }
    else
    {
        cout << "Negative number " << endl;
    }
    if (num % 2 == 0 )
    {
        cout << "Even number " << endl;
    }
    else
    {
        cout << "odd number" << endl;
    } 
    
    // Version 2
    if (num >0)
    {
        cout << "positive number " << endl;
        if ( num % 2 == 0)
        {
            cout << "Even number " << endl;
            
        }
        else 
        {
            cout << "odd number " << endl;
        }
        else
        {
            cout << "negative # " << endl;
        } */
    
    
    
    // For loop
    // purpose is to iterate an "n" number of times
    //want to iterate 10 times in code below
  /*  for (int i = 0 ; i < 10; i++)
    {
        cout << i << endl;
    }
    */
    
    // User controlled loop
    // prompt 
    cout << " enter a # . -1 to quit " << endl;
    int num;
    cin >> num;
    while (num != -1 )
    {
        cout << "you entered: " << num << endl;
        cout << "Enter a different #. -1 to quit " << endl;
        // has to be input inside
        cin >> num;
        
    }
    
    
    
    return 0;
}

