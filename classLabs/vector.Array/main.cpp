/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on April 29, 2014, 11:39 AM
 */

#include <cstdlib>
#include <iostream>
#include <vector>
using namespace std;



//proto type for output function
void output(const vector<int> &);
/*
 * 
 */
int main(int argc, char** argv) {
    
    vector<int> v;// empty vector
    v.push_back(3);
    v.push_back(6);
    v.push_back(8);
    
    output(v);
    
    
    
    

    return 0;
}
/*
 *the output function outputs the contents of a type
 * type of integer
 * return void
 * parameters is one vector of type integer
 *  
 */

void output(const vector<int> &v)
{
   for(int i = 0; i < v.size(); i++)
    {
       cout << v[i] << " ";
        
    }   
    cout << endl;
    
}
