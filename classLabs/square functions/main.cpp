/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 27, 2014, 10:46 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;
// square (define) the square function
// square function returns the square value
// needs 1 parameter, an integer

int square (int num)
{
    return num * num;
}



// define problem 1
void problem1 ()
{
    cout << "Enter a number: " << endl;
    int value;
    cin >> value;
    
    int squaredValue = square(value);
    cout << "The square of " << value
         << " is : " << squaredValue << endl;
    // outputting the function call
     cout << "The square of " << value
          << " is : " << square (value)<< endl;
     //using ineterger literal
    
}


// define end program function
void endProgram ()
{
    cout << "Your program ended " << endl;
}

int main(int argc, char** argv) {
   cout << "Enter a number: " << endl;
    int value;
    cin >> value;
    int squaredValue = square(value);
    cout << "The square of " << value 
         << " is : " << squaredValue << endl;
    // outputting the function call
     cout << "The square of " << value
          << " is : " << square(value)<< endl;
   
    int select;
    do 
    {
    cout << "Enter a number: " << endl;
    cout << "1 for square value " << endl;
    cout << "-1 to end" << endl;
    
    cin >> select;
    
    switch (select)
      {        case 1:
                problem1();
                break;
               case -1:
                endProgram();
            break;
      }    
    }
    while (select != -1);

    
    return 0;
}