/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 27, 2014, 10:52 AM
 */

#include <cstdlib>
#include <iostream> // input/output
#include <iomanip>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
        
        // const variables to store a number
    const double randNum = 12.3259; 
    
        // error cant reassign constant values
    //randNumb = 12.5;
    
    // change number of numbers to 2
    // no fixed means no decimal set
    
    cout << setprecision(2);
    
    cout << randNum << endl; 
    // output a three digit number
    // only see 2 digits with scientific notation
    cout << 120.592 << endl;
    
    // fixing means change decimal place
    cout << fixed << randNum << endl;
    
    //use set w to create columns 
    // set w only manipulates very next output
    // set with automatically right  justifies
    cout << setw(10) << randNum << randNum;
    
    //using left justification 
    //setw reserves spaces for next output 
    cout << setw(10) << left << randNum << randNum << endl;
    
    //using setfill 
    cout << setw(10)
        << setfill ('=')
        << randNum
        << randNum
        << endl;
        
        
              
    
    

    return 0;
}

