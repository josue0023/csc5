/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 18, 2014, 11:29 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    
    //create a random number
    // provide a time to the random # generator
    //srand returns no value 
    srand(time(0));
    
    int randNum = rand();
    //get range between 0-100
    cout << "Number is: " << randNum << endl;
     int randNum2 = rand() % 101;
    cout << "Number 2 is: " << randNum2 << endl;
    //get range 100-1000
    //store everything in variables
    int max = 1000;
    int min = 100;
    int randNum3 = rand() % (max - min + 1 ) + min;
    
    cout << "Number 3 is : " << randNum3 << endl;
    //find out how many iterations takes to get # 1000
    //use counter to count the iterations
    
    int counter = 1;
    while (randNum3 != 1000)
    {
        counter++;
        cout << "Iteration: " << counter << endl;
      randNum3 = rand() % (max - min + 1 ) + min;
    }
    
    

    return 0;
}

