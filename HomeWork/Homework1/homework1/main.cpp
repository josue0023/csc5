/*
* Name: Josue Figueroa
* Student ID:2505016
* Date:2/26/2014
* HW:1
* Problem:1
* I certify this is my own work and code
*/

#include <cstdlib>
#include <iostream> 
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
        cout << "Hello \n ";
    int number_of_pods, peas_per_pod, total_peas;
	cout << "Press return after entering a number. \n";
	cout << "Enter the number of pods:\n";
	cin >> number_of_pods;
	cout << "Enter the  number of peas in a pod:\n";
	cin >> peas_per_pod;
	total_peas = number_of_pods + peas_per_pod;
	cout << "If you have ";
	cout << number_of_pods;
        cout << " pea pods\n";
	cout << "and ";
	cout << peas_per_pod;
	cout << " peas in each pod, then\n";
	cout << "you have ";
	cout << total_peas;
	cout << " peas in all the pods.\n";
        cout << "Goodbye \n";
    return 0;
}
