/* 
* Name: Josue Figueroa
* Student ID:2505016
* Date:2/26/2014
* HW:1
* Problem:5
* I certify this is my own work and code
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    int number1, number2, sum, product;
    cout << "Enter the first number:\n";
    cin >> number1;
    cout << "Enter the second number:\n";
    cin >> number2;
    cout << "sum of the first number and second number is:\n";
     sum = number1 + number2; 
    cout << sum;
    cout << ".\n";
    cout << "product of the first number and second number is:\n";
    product = number1 * number2;
    cout << product;
    cout << ".\n";
    cout << "This is the end of the program";
    
    
    
    return 0;
}

