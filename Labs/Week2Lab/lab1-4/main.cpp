/* 
* Name: Josue Figueroa
* Student ID:2505016
* 
 *
 * Created on February 27, 2014, 11:51 AM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
using namespace std;

/*
 * 
 */ // problem 4 of week 2 worksheet 
int main(int argc, char** argv) {
    double Inch = 12;
    double mile = 1609.344;
    double feet = 3.281; 
    double NumberOfMeters;
    
    
    cout << " Enter the amount of Meters. " << endl;
    cin >> NumberOfMeters;
    
    cout << NumberOfMeters << " equivalent ";
    cout << NumberOfMeters / mile;
    cout << " mile , " ;
    cout << NumberOfMeters * feet;
    cout << " feet, and ";      
    cout << NumberOfMeters * feet * Inch;
    cout << " Inches ";
    return 0;
}

