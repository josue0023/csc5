/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 27, 2014, 11:31 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */ //problem 2 of week 2, part 1 worksheet 
int main(int argc, char** argv) {
    int a = 30;
    int b = 10;
    // need a temporary variables to store value of a
    //a's value gets reassigned and is never stored anywhere else
    int temp = a;
    cout << " a: " << a << " b: " << b << endl;
    
    cout << " b: " << b << " a: " << a << endl;
    
    a = b; // Lose the data of a
    b = temp;  

    return 0;
}

