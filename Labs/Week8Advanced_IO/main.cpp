/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on April 22, 2014, 10:56 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    //use cin.get() function to get a single character
    char c;
    cout << "enter a character: " << endl;
    c = cin.get();
    cout << "Your character isL " << c << endl;
    
    
    
    
    
    
    
    
    //get line to get a sentence from user
    // getline reads until the newline character
    string sentence;
    cout <<"please enter a sentence " << endl;
    //getline needs a buffer, and a string
    getline(cin,sentence);
    cout <<"Your sentence is: " << sentence << endl;
    
    //stream insertion
    //stream insertion leaves newline in the buffer
    // try not to mix >> and getline
    cout <<"Please enter a word " << endl;
    string word;
    cin >> word;
    cout << "your word is: " << word << endl;
    
    //CLEANING FIXING THE BUFFER
    while (true)
    {
        int number;
        cout << "Enter a number: ";
        cin >> number;
        cout << "Your number is: " << number << endl;
        //string break buffer
        //need to fix and reset the cin buffer
        if (cin.fail()) //need to check if buffer broke
        {
            //need to reset the buffer
            //clear the buffer
            cin.clear(); //resets the buffer
            cin.ignore(256, '\n');//ignore (clear)
        }
    }
    
    
    
    
    

    return 0;
}

