/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on April 22, 2014, 11:44 AM
 */

#include <cstdlib>
#include <iostream>
#include <vector>
using namespace std;
int getNumber()
{
    cout << "Please enter a number: " << endl;
    int num;
    cin >> num;
    return num;
}

/*
 * 
 */
int main(int argc, char** argv) {
    // get 10 values from the user & insert it into a vector 
    // method 1 - empty vector
    vector<int> emptyvector;
    //output vector before
    for (int i = 0; < emptyvector.size(); i++)
    {
        cout << emptyvector[i] << endl;
    }
    //always use for loops for vectors
    for (int i = 0; i < 10; i++)
    {
        // write a function for user input
        int num = getNumber;
        emptyvector.push_back(num);
    }
    
    
    //method 2
   vector<int> v(10); 
  cout << endl << "Before vector: " << endl;
    for(int i = 0; i < v.size(); i++)
    {
        // Using subscript operator
        cout << v[i] << endl;
    }
    
    for (int i = 0; i < v.size(); i++)
    {
        v[i] = getNumber();
    }
    
    cout << endl << "After vector: " << endl;
    for(int i = 0; i < v.size(); i++)
    {
        // Using subscript operator
        cout << v[i] << endl;
    }

    return 0;
}

