/* 
 * File:   main.cpp
 * Author: josue
 *
 * Created on March 12, 2014, 4:25 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    int singles;
    int doubles;
    int triples;
    int homeruns;
    int at_bats;
    
    cout << "Enter number for singles ";
    cin >> singles;
    cout << "Enter number for doubles ";
    cin >> doubles;
    cout << "Enter number for triples ";
    cin >> triples;
    cout << "Enter number for homeruns ";
    cin >> homeruns;
    cout << "Enter number greater then singles,doubles,triples and homeruns ";
    cin >> at_bats;
    cout <<"Slugging percentage = " 
         <<singles + 2 * doubles + 3 * triples + 4 * homeruns / at_bats;
    

    return 0;
}

