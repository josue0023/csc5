/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on May 22, 2014, 11:57 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;
int** createMultiDynamicArray(int row, int col)
{
    

int** p = new int*[row];

for ( int i = 0; i < row; i++)
  {
  p[i] = new int[col];  
  }
return p;
}



void output (int** p, int row, int col)
{
    for (int i = 0; i < row; i++)
    {
        for (int j = 0; j< col; j++)
            cout << p[i][j] << "  ";
        cout << endl;
    }
}

typedef int* Intptr;

void fillvalues(Intptr*p, int row, int col)
{
    for(int i = 0; i < row; i++)
    {
        for(int j = 0; j < col; j++)
        {
            p[i][j] = i * col + j + 1;
        }
    }
}


/*
 * 
 */
int main(int argc, char** argv) {
    
   int row = 5;
   int col = 3;
   Intptr*p = createMultiDynamicArray(row, col);
   output(p, row , col);
   
   fillvalues(p,row,col);
   output(p,row,col);
   
           
           
    
    
    

    return 0;
}

