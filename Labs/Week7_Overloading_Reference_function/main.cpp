/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on April 1, 2014, 11:52 AM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

//create a global constant for PI
//global outside main
const double PI = 3.14;
//function prototype / declaration
double circumference(double);
double area (double);



/*
 * function for prototype for swap
 * 
 */
void swapNonReferenced(double, double);
void swapReferenced (double&, double&);
void output (double, double);
int main(int argc, char** argv) {
    
    cout << "Enter a radius: " << endl;
    int radius;
    cin >> radius;
    cout << "The circumference is: "
         << circumference(radius) << endl;
    cout << "The area is: "
         << area(radius) << endl;
    
    cout << "Enter 2 values:" << endl;
   double x,y;
    cin >> x >> y;
    output (x, y);
    swapNonReferenced(x,y);
    output (x, y);
    
    cout  << "Using reference" << endl;
    
    output (x, y);
    swapReferenced(x,y);
    output (x,y);
           
    
           
    
    

    return 0;
}

// define all my function that are prototyped 
/*
 * function comments  need 3 keys details
 * 1. High level view describing what the function is supposed to do
 * circumfrence function that calculates
 * the circumfrence of a circle 
 * 
 * 2.(return type)fuctioon return the circunfrence of a given radius type double
 * 
 * 3.(parameters) one parameters of type double 
 * that represents the radius of a circle
 */
double circumference (double r)
{
    return 2 * PI * r;
}
/*
 * returns a double representing area of circle
 * 
 * 1. area function returns the area or a circle with a given radius
 * 3. one parameter which is the radius circle of type double
 */
double area (double r)
{
    return PI * r * r;
}
/*
 *swap nonreference attempts to swap two values without
 * referenced the parameters
 * returns nothing
 * parameters: two doubles representing the values swap
 * 
 */
void swapNonReferenced (double x, double y)
{
    int temp = x;
    x = y;
    y = temp;
}
void swapReferenced (double &x, double &y)
{
    int temp = x;
    x = y;
    y = temp;
}
void output (double x, double y)
{
    cout << "First: " << x
         << "Scond: " << y << endl;   
}